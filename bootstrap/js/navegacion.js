

/**
 * Esta variable es critica para la construccion del carousel y del menú, es importante que las epocas de salsa lleven el mismo nombre siempre 
 * Salsa, Presalsa, Siglo XXI, su estructura no puede variar y debe tener definido los atributos necesarios (id, label etc.. )
 */

var Navegation =
{
    label: 'Principal',
    subs:
    [
    {
        label: 'Cali',
        id: 'Cali',
        srcCarouselFondo:'Carousel/images/caraCali.jpg' ,  
        subs: [
        {
            label: 'Juanchito',
            id: 'Juanchito',
            subs: [
            {
                label: 'Salsa',
                url: 'Cali/Juanchito/Salsa/escenario.html'
            },
            {
                label: 'Siglo XXI',
                url: 'Cali/Juanchito/SigloXXI/escenario.html'
            }
            ]
        },
        {
            label: 'Barrio1',
            id: 'Barrio1',
            subs: [
            {
                label: 'Presalsa',
                url: 'Cali/Barrio1/Presalsa/escenario.html'
            },
            {
                label: 'Salsa',
                url: 'Cali/Barrio1/Salsa/escenario.html'
            }
            ]
        },
        {
            label: 'Barrio2',
            id: 'Barrio2',
            subs: [
            {
                label: 'Siglo XXI',
                url: 'Cali/Barrio2/SigloXXI/escenario.html'
            }
            ]
        },
        {
            label: 'Barrio3',
            id: 'Barrio3',
            subs: [
            {
                label: 'Siglo XXI',
                url: 'Cali/Barrio3/SigloXXI/escenario.html'
            }
            ]
        },                          
        {
            label: 'Calles Salsa',
            id: 'CallesSalsa',
            subs: [
            {
                label: 'Salsa',
                url: 'Cali/CallesSalsa/Salsa/escenario.html'
            },
            {
                label: 'Siglo XXI',
                url: 'Cali/CallesSalsa/SigloXXI/escenario.html'
            }
            ]
        },
        {
            label: 'Zona de tolerancia',
            id: 'ZonaTolerancia',
            subs: [
            {
                label: 'Presalsa',
                url: 'Cali/ZonaTolerancia/Presalsa/escenario.html'
            }
            ]
        }
                            

        ]
    },
    {
        label: 'Nueva York',
        id: 'NuevaYork',
        srcCarouselFondo:'Carousel/images/caraNewYork.jpg' , 
        subs: [
        {
            label: 'Barrio',
            id: 'BarrioNY',
            subs:[
            {
                label: 'Presalsa',
                url: 'NuevaYork/Barrio/Presalsa/escenario.html'
            },
            {
                label: 'Salsa',
                url: 'NuevaYork/Barrio/Salsa/escenario.html'
            }
            ]
        },
        {
            label: 'Louizaida',
            id: 'Louizaida',
            subs:[
            {
                label: 'Siglo XXI',
                url: 'NuevaYork/Louizaida/SigloXXI/escenario.html'
            }
            ]
        },
        {
            label: 'Bronx',
            id: 'Bronx',
            subs:[
            {
                label: 'Presalsa',
                url: 'NuevaYork/Bronx/Presalsa/escenario.html'
            },
            {
                label: 'Salsa',
                url: 'NuevaYork/Bronx/Salsa/escenario.html'
            }
            ]
        },
        {
            label: 'Brooklyn',
            id: 'Brooklyn',
            subs:[
            {
                label: 'Salsa',
                url: 'NuevaYork/Brooklyn/Salsa/escenario.html'
            }
            ]
        },
        {
            label: 'Spanish Harlem',
            id: 'SpanishHarlem',
            subs:[
            {
                label: 'Siglo XXI',
                url: 'NuevaYork/SpanishHarlem/SigloXXI/escenario.html'
            }
            ]
        }
        ]
    },
    {
        label: 'Puerto Rico',
        id:'PuertoRico',
        srcCarouselFondo:'Carousel/images/caraPuerto.jpg', 
        subs:[
        {
            label: 'San Juan',
            id: 'SanJuan',
            subs:[
                
            {
                label: 'Presalsa',
                url: 'PuertoRico/SanJuan/Presalsa/escenario.html'
            },
            {
                label: 'Salsa',
                url: 'PuertoRico/SanJuan/Salsa/escenario.html'
            },
            
            
            {
                label: 'Siglo XXI',
                url: 'PuertoRico/SanJuan/SigloXXI/escenario.html'
            }
            ]
        },
        {
            label: 'Calle Calma',
            id: 'CalleCalma',
            subs:[
            {
                label: 'Siglo XXI',
                url: 'PuertoRico/CalleCalma/SigloXXI/escenario.html'
            }
            ]
        },
        {
            label: 'Santurce',
            id: 'Santurce',
            subs:[
            {
                label: 'Salsa',
                url: 'PuertoRico/Santurce/Salsa/escenario.html'
            }
            ]
        },
        ]
    },
    {
        label: 'Cuba',
        id:'Cuba',
        srcCarouselFondo:'Carousel/images/caraCuba.jpg' , 
        subs:[
        {
            label: 'Habana',
            id: 'Habana',
            subs:[
            {
                label: 'Presalsa',
                url: 'Cuba/Habana/Presalsa/escenario.html'
            },
            {
                label: 'Siglo XXI',
                url: 'Cuba/Habana/SigloXXI/escenario.html'
            }
            ]
        }
        ]
    },
    {
        label: 'Curazao',
        id:'Curazao',
        srcCarouselFondo:'Carousel/images/caraNewYork.jpg' , 
        subs:[
        {
            label: 'Curazao',
            id: 'Curazao1',
            subs:[
            {
				label: 'Siglo XXI',
				url: 'Curazao/Curazao/SigloXXI/escenario.html'
			}
            ]
            
        }
        ]
    }
    ]
};


function contruirCollapsibleNavigation() {

    var collap = $("#accordion_parent");



    for (x in Navegation.subs)
    {

        var ciudad = Navegation.subs[x];
        var accordiGroup = $("<div  class='accordion-group'></div>");


        accordiGroup.append(
            "<div class='accordion-heading'>" +
            "<a class=' btn  accordion-toggle' data-toggle='collapse' data-target='#accordion_" + ciudad.id + "' data-parent='#accordion_parent' >" +
            ciudad.label +
            "</a>" +
            "</div>");



        var accordinBody = $('<div id="accordion_' + ciudad.id + '" class="accordion-body collapse ">' +
            ' </div>  ');
        var innerAccordion = $('<div class="accordion-inner"></div>');

        var collap2 = $('<div class="accordion" id="accordion_parent_' + ciudad.id + '"></div>');

        for (x2 in ciudad.subs) {
            var lugares = ciudad.subs[x2];
            var accordiGroup2 = $("<div  class='accordion-group'></div>");
            accordiGroup2.append(
                "<div class='accordion-heading'>" +
                "<a class=' btn  accordion-toggle' data-toggle='collapse' data-target='#accordion_" + ciudad.id + "_" + lugares.id + "' data-parent='#accordion_parent_" + ciudad.id + "' >" +
                lugares.label +
                "</a>" +
                "</div>");


            var accordinBody2 = $('<div id="accordion_' + ciudad.id + "_" + lugares.id + '" class="accordion-body collapse ">' +
                '  </div>  ');



            var innerAccordion2 = $(' <div class="accordion-inner"></div> ');
            var listLinks = $("<ul class='nav nav-list'></ul>");

            for (x3 in lugares.subs) {

                var etapaSalsa = lugares.subs[x3];

 
                var liItem = $('<li ><a href="#" onclick="javascript:cambiarPagina(\'' + etapaSalsa.url + '\')">' + etapaSalsa.label + '</a></li>');

                listLinks.append(liItem);

            }

            innerAccordion2.append(listLinks);
            accordinBody2.append(innerAccordion2);

            accordiGroup2.append(accordinBody2);


            collap2.append(accordiGroup2);


        }
        innerAccordion.append(collap2);
        accordinBody.append(innerAccordion);
        accordiGroup.append(accordinBody);


        collap.append(accordiGroup);


    }

    collap.append('  <br /> <button type="button" id="closeNav" onclick="javascript:toggleNavPanel()" class="btn " >' +
        ' <i class="  icon-hand-left"></i>' +
        '</button>');

}

contruirCollapsibleNavigation();

$(".accordion-toggle").append("<i style='float: right;' class=' icon-white icon-chevron-down'> </i>");

$("#accordion_parent ul li ").click(function(e) {

    $("#accordion_parent ul li ").removeClass("active");
    $(this).addClass("active");

});
