

/**
 * @author Luis Felipe Vargas <felipe.vargas@correounivalle.edu.co>
 * @class
 * @classdesc audioCustom es una clase, dedicada a las funciones necesarias para los elementos audio, en la multimedia.
 * 
 */

var audioCustom= function (){
    
    /**
     * Es un arreglo con los nombres de las canciones que se reproducen 
     * al iniciar la multimedia, la reproducción se lleva a cabo automaticamente.
     * @type array
     */
    this.randSongs=  ["JuanitoAlimaMa.mp3", "LaMurga.mp3", "PeriodiDeAyer.mp3",  "PlatoDeSegundaMesa.mp3"];
    
    
    /**
     * Es una función la cual selecciona aleatoriamente 
     * un nombre de la la lista randSongs y lo reproduce
     * a través del objeto audio.
     * @type Function
     * @param {String} id  id es un texto, con el id del componente audio.
     * 
     */
    this.playRand =function (id) {
        
        console.log(this.randSongs);

        var audio = document.getElementById(id);        
        while (audio.firstChild) {
            audio.removeChild(audio.firstChild);
        }

        var source = document.createElement('source');    
        source.type = 'audio/mpeg';
        source.src = 'musica/' + this.randSongs[ Math.floor((Math.random() * 4))];
        audio.appendChild(source);  
        
        this.validateCanPLay(audio);
        audio.play();
        
                             
    }
    
   
    /**
     * Es una función la cual verifica si el navegador, soporta los formatos de los sonidos disponibles
     * @type Function
     * @param {Object} audioObject  audioObject es un objeto audio, el cual se  verifica si se  puede reproducir.
     * 
     */
  
    this.validateCanPLay=function (audioObject) {
        
      
        isSuppMp3=audioObject.canPlayType('audio/mpeg');
      
      
       
        
        if (!isSuppMp3) {
            console.log('No se puede reproducir el audio. Por favor verifique si su navegador soporta MP3');          
            $('body').append('<div class="alert alert-warning fade in">'+
                ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'+
                '<strong>Warning!</strong> Su navegador no soporta audios en formato mp3.'+
                '</div>'); 
                                 
        } else {      
            console.log('success');      
            
        }
       
    }
 
}

var audioCustomObject= new audioCustom(); 

/*
module.exports={
    audioCustom:audioCustom
    
    
}*/