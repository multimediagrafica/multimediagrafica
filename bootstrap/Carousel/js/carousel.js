/*!
 * jQuery Mobile Carousel  Widget v 1.0.1
 * Developed by Rebrov Andrey
 * http://andrebrov.net/
 *
 * Dual licensed under the MIT or GPL Version 2 licenses.
 */


if (!$.widgets) {// cuando esté indefinido haga lo siguiente:
    //console.dir($.widgets);
    $.widgets = function() {
        var getDimensions = function() {

            return {
                h: "100%",
                w: "100%"
            };
        };
        return {
            getDimensions: getDimensions
        };
    }();

//console.dir($.widgets);
}

$.widgets.Carousel = function(wargs) {
    console.dir(this);
    var _widget = this;
    var items = [];
    var index = 0;
    var showNav = true;
    var itemHeight = 100;
    var itemWidth = 10;
    var baseWidth = "100%";
    var bloqueSize = 0;// porciento
    _widget.node = $("#" + wargs.uuid);
    var template;
    var scrollInterval = 1000;
    touchStartEvent = $.support.touch ? "touchstart" : "mousedown";
    touchStopEvent = $.support.touch ? "touchend" : "mouseup";
    touchMoveEvent = $.support.touch ? "touchmove" : "mousemove";


    this.addItem = function(item) {

        //console.dir(item);
        if (!item.id)
            item.id = items.length;
        var id = item.id;

        item.ancho = ((100 / wargs.value.length)) + "%";
        //alert("ancho "+item.ancho);
        //alert("size bloque"+bloqueSize);
        var text = _widget.applyTemplate(item, template);

        //var item_content=$(text) ;

        //item_content.width((100/wargs.value.length)+"%" );

        var div_item_content = $("<div id='" + wargs.uuid + "_item_" + item.id + "'>" + text + "</div>");


        //var porcetajeAncho=100/wargs.value.length;

        //alert(itemWidth);
        div_item_content.css('display', 'inline');
        //div_item_content.addClass("carousel-content");

        div_item_content.appendTo(_widget.container);
        /*.css("zIndex", 1).css("background", "blue").width(itemWidth).height(itemHeight).css('display', 'inline').css("float", "left")*/
        $("#" + wargs.uuid + "_item_" + id).height(itemHeight);
        item.div = $("<div id='" + wargs.uuid + "_item_" + item.id + "'>" + text + "</div>");
        items.push(item);


        var _sel = $("<div id='" + id + "'></div>");
        _sel.bind(touchStopEvent, function(e) {
            _widget.select(e.target.id);
        });
        _sel.append((id + 1)).removeClass().addClass("carousel-id ");
        _widget.mid.append(_sel);
        item.menu = _sel;
        return id;


    };
    this.select = function(itemId) {
        //alert('addItem ' +itemId);
        for (var _i = 0; _i < items.length; _i++) {
            var item = items[_i];
            if (item.id == itemId) {
                _widget.showIndex(_i);
                item.menu.addClass("carousel-id carousel-id-theme carousel-id-selected-theme");
            } else {
                item.menu.removeClass().addClass("carousel-id carousel-id-theme");
            }
        }
    };
    this.applyTemplate = function(obj, _t) {

        for (var i in obj) {
            var token = "@{" + i + "}";
            while (_t.indexOf(token) != -1) {
                _t = _t.replace(token, obj[i]);
            }
        }
        return _t;
    };
    this.showIndex = function(targetIndex) {

        if (targetIndex < index ||
                targetIndex >= index + 1) {
            if (targetIndex < index) {
                var _cb = function() {
                    index = targetIndex;
                };
                var targetPos = bloqueSize + (targetIndex * bloqueSize * -1);// muevalo a la izquierda
                doScroll(targetPos, _cb, targetIndex);
            } else if (targetIndex >= index + 1) {
                var callback = function() {
                    index = targetIndex;
                };
                var tp = bloqueSize + (targetIndex * bloqueSize) * -1;
                doScroll(tp, callback, targetIndex);
            }
        }
    };
    function doScroll(target, callback, targetIndex) {
        //alert(_widget.container.css('left'));

        console.log(target);

        //$('div.carousel-item').css('width', ((100/wargs.value.length)-5)+"%");
        //$('div.carousel-item').eq(targetIndex).css('position',"inherit");
        _widget.container.animate({
            left: target + "%"
        }, scrollInterval, function() {
            callback();

            $('div.carousel-item').removeClass("scale1");
            $('div.carousel-item').removeClass("scale0_3");
            $('div.carousel-item').addClass("scale0_3");

            $('div.carousel-item').eq(targetIndex).removeClass("scale0_3");
            $('div.carousel-item').eq(targetIndex).addClass("scale1");


        });





        /* $('div.carousel-item').eq(targetIndex).animate({
         width:"40%"
         }, scrollInterval, function() {
         //$('div.carousel-item').css('opacity', 1);
         });*/
    }

    this.getNext = function() {
        if (index < items.length - 1) {
            var _jumpTo = index + 1;
            if (_jumpTo > items.length - 1) {
                _jumpTo = items.length - 1;
            }
            _widget.select(items[_jumpTo].id);
        }
    };
    this.getPrevious = function() {
        if (index >= 1) {
            var _jumpTo = index - 1;
            if (_jumpTo < 0) {
                _jumpTo = 0;
            }
            _widget.select(items[_jumpTo].id);
        }
    };
    this.setItems = function(_in) {
        var data = _in;
        //_widget.container.width((itemWidth + 10) * data.length );

        //alert("args ::" + wargs.value.length);
        var sizeContainer = (((wargs.value.length / 3) * 100));
        //alert("conteines "+sizeContainer);
        bloqueSize = (sizeContainer / wargs.value.length);//ZONA CRITICA
        console.error(bloqueSize);

        _widget.container.css('left', (bloqueSize) + '%');
        
        
        _widget.container.width(sizeContainer + "%");
        for (var _i = 0; _i < data.length; _i++) {
            _widget.addItem(data[_i]);
        }
        _widget.mid.width(data.length * 30);

        if (data.length && data.length > 0) {
            _widget.select(0);


            $('div.carousel-item').eq(0).addClass("scale1");
            $('div.carousel-item').not(".scale1").addClass("scale0_3");

        }
    };
    this.load = function() {

        _widget.init();
        if (wargs.value) {// si tiene imagenes haga esto
            var data = wargs.value;
            _widget.setItems(data);
        }
    };
    this.init = function() {
        if (!_widget.node)
            return;// para esta entrada carousel
        _widget.container = $("#" + wargs.uuid + "_content");
        _widget.scrollpane = $("#" + wargs.uuid + "_scrollpane");

        _widget.node.bind(touchStartEvent, function(e) {
            //previene que haga la accion por defecto que es arrastrar la imagen
            //_widget.touches = [];

            e.preventDefault();
        });



        
           
            _widget.container.swipe({
                //Generic swipe handler for all directions
                swipe: function(event, direction, distance, duration, fingerCount) {
                    console.log("You swiped " + direction);
                    if (direction==="right"){
                        _widget.getPrevious();
                        
                    } else if (direction==="left") {
                         _widget.getNext();
                        
                    }
                },
                //Default is 75px, set to 0 for demo so any distance triggers swipe
                threshold: 0
            });
        
       /* _widget.container.bind("swipeleft", function(e) {
            _widget.getNext();
            console.log('left');
           

        });
        _widget.container.bind("swiperight", function(e) {
            console.log('rigth');

            console.dir(_widget.getPrevious());
            _widget.getPrevious();
        });*/
        _widget.node.bind(touchStopEvent, function(e) {

            e.preventDefault();
        });


        _widget.nav = $("#" + wargs.uuid + "_nav");
        _widget.mid = $("#" + wargs.uuid + "_mid");
        //_widget.node.addClass("carousel-theme");
        template = unescape($("#" + wargs.uuid + "_template").html() + "");

        //_widget.container.css("left", "0");
        _widget.resize();
    };

    this.resize = function() {
        var _dim = $.widgets.getDimensions();
        var _w = _dim.w;
        //_widget.scrollpane.width(_w);
        itemWidth = 400;



        /*if (/%/i.test(baseWidth)) {
         var _wInterna = new Number(baseWidth.split('%')[0]);
         alert("w interna "+_wInterna);
         itemWidth = (_dim.w) / _wInterna * 100;
         alert("entro "+itemWidth);
         } else {
         itemWidth = baseWidth;
         alert("entro else "+itemWidth);
         }*/


        /*for (var i = 0; items && i < items.length; i++) {
         
         
         $(items[i]).find("div").width(itemWidth).height(itemHeight);            
         }*/
        //_widget.node.height(_dim.h - 12);

        _widget.nav.css("display", "block");
        //itemHeight = _dim.h - 42;            
        _widget.mid.width(items.length * 30);
        //_widget.scrollpane.height(_dim.h);

    };
    _widget.load();//primer evento llamado
};